FROM openjdk:13
ENV LAUNCHER_VERSION=0.0.1
COPY ./target/hab88launcher-$LAUNCHER_VERSION.jar /hab88launcher.jar
ENTRYPOINT ["java", "-jar", "hab88launcher.jar"]
