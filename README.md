### Install ###

* [Install docker](https://docs.docker.com/engine/installation/)
* [Install docker-compose](https://docs.docker.com/compose/install/)
* [Install maven](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)


### Build/Run ###

**Build/Run project in container**
```
cd {PROJECT_HOME}
mvn clean install
docker-compose up -d
```