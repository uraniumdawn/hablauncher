package io.pmlab.hab88launcher.advice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.pmlab.hab88launcher.model.LaunchApiError;
import io.pmlab.hab88launcher.model.ValidationError;

@RestControllerAdvice
public class LaunchRestExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public LaunchApiError handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<ValidationError> validationErrors = new ArrayList<>();
        ex.getBindingResult()
                .getAllErrors()
                .forEach(error -> {
                    String fieldName = ((FieldError) error).getField();
                    String errorMessage = error.getDefaultMessage();
                    validationErrors.add(new ValidationError(fieldName, errorMessage));
                });
        return new LaunchApiError(HttpStatus.BAD_REQUEST, LocalDateTime.now(), "Request parameters validation errors",
                validationErrors);
    }
}
