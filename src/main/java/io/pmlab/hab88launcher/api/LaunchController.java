package io.pmlab.hab88launcher.api;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.pmlab.hab88launcher.model.LaunchRequestDto;
import io.pmlab.hab88launcher.model.LaunchResponseDto;
import io.pmlab.hab88launcher.service.LaunchService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class LaunchController {

    private LaunchService service;

    @PostMapping(value = "/launch/real", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public LaunchResponseDto provideUrl(@RequestBody @Valid LaunchRequestDto requestDto) {
        return service.provideUrl(requestDto);
    }


}
