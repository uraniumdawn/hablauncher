package io.pmlab.hab88launcher.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.pmlab.hab88launcher.utils.ValidationErrorMessages;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LaunchRequestDto {

    @NotEmpty(message = ValidationErrorMessages.PROVIDER_GAME_ID_EMPTY_OR_NULL_VALUE)
    @JsonProperty("providerGameId")
    private String providerGameId;

    @JsonProperty("sessionKey")
    private String sessionKey;

    @NotEmpty(message = ValidationErrorMessages.CHANNEL_NULL_VALUE)
    @Pattern(regexp = "mobile|desktop", message = ValidationErrorMessages.CHANNEL_INCORRECT_VALUE)
    @JsonProperty("channel")
    private String channel;

    @NotEmpty(message = ValidationErrorMessages.LANG_EMPTY_OR_NULL_VALUE)
    @JsonProperty("lang")
    private String lang;

    @NotEmpty(message = ValidationErrorMessages.LOBBY_URL_EMPTY_OR_NULL_VALUE)
    @URL(message = ValidationErrorMessages.LOBBY_URL_INCORRECT_VALUE)
    @JsonProperty("lobbyUrl")
    private String lobbyUrl;
}
