package io.pmlab.hab88launcher.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LaunchResponseDto {
    @JsonProperty("url")
    private String url;
}
