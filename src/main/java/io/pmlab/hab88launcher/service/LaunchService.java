package io.pmlab.hab88launcher.service;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

import io.pmlab.hab88launcher.model.LaunchRequestDto;
import io.pmlab.hab88launcher.model.LaunchResponseDto;

@Service
public class LaunchService {

    private final String url;
    private final String gameId;

    public LaunchService(@Value("${hab88launcher.habanero.api.url}") String url,
            @Value("${hab88launcher.gameid}") String gemeId) {
        this.url = url;
        this.gameId = gemeId;
    }

    public LaunchResponseDto provideUrl(LaunchRequestDto requestDto) {

        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("brandid", requestDto.getProviderGameId());
        params.add("brandgameid", gameId);
        if (requestDto.getSessionKey() != null) {
            params.add("token", requestDto.getSessionKey());
        }
        params.add("mode", requestDto.getSessionKey() != null ? "real" : "fun");
        params.add("locale", requestDto.getLang());
        params.add("lobbyurl", UriUtils.encode(requestDto.getLobbyUrl(), StandardCharsets.UTF_8));

        var uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParams(params);
        return new LaunchResponseDto(uriComponentsBuilder.build()
                .toUriString());
    }
}
