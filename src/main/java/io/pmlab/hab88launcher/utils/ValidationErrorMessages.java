package io.pmlab.hab88launcher.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidationErrorMessages {
    public static final String PROVIDER_GAME_ID_EMPTY_OR_NULL_VALUE = "Game provider ID shouldn't be null of empty";
    public static final String CHANNEL_NULL_VALUE = "Channel shouldn't be null";
    public static final String CHANNEL_INCORRECT_VALUE = "Channel should be any of [mobile, desktop]";
    public static final String LANG_EMPTY_OR_NULL_VALUE = "Lang shouldn't be null of empty";
    public static final String LOBBY_URL_EMPTY_OR_NULL_VALUE = "Lobby URL shouldn't be null of empty";
    public static final String LOBBY_URL_INCORRECT_VALUE = "Lobby URL should by a valid URL";
}
