package io.pmlab.hab88launcher.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.pmlab.hab88launcher.model.LaunchRequestDto;
import io.pmlab.hab88launcher.model.LaunchResponseDto;
import io.pmlab.hab88launcher.service.LaunchService;
import io.pmlab.hab88launcher.utils.ValidationErrorMessages;

@WebMvcTest(LaunchController.class)
class LaunchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LaunchService service;

    @DisplayName("Perform successfully")
    @Test
    void shouldPerformSuccessfully() throws Exception {
        when(service.provideUrl(any(LaunchRequestDto.class))).thenReturn(
                new LaunchResponseDto("https://www.google.com"));

        mockMvc.perform(post("/launch/real").content(new ObjectMapper().writeValueAsString(
                new LaunchRequestDto("gameId", "sessionKey", "mobile", "ua", "https://lobby.com/path?param=1")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.url").exists());
    }

    @DisplayName("Fail with empty/null providerGameId value")
    @ParameterizedTest
    @NullAndEmptySource
    void shouldFailWithBlankOrNullProviderGameId(String providerGameId) throws Exception {
        var requestDto = new LaunchRequestDto(providerGameId, "sessionKey", "mobile", "ua",
                "https://lobby.com/path?param=1");

        performAndAssert(requestDto, "providerGameId", ValidationErrorMessages.PROVIDER_GAME_ID_EMPTY_OR_NULL_VALUE);
    }

    @DisplayName("Fail with empty/null channel value")
    @ParameterizedTest
    @NullSource()
    void shouldFailWithNullOrEmptyChannel(String channel) throws Exception {
        var requestDto = new LaunchRequestDto("providerGameId", "sessionKey", channel, "ua",
                "https://lobby.com/path?param=1");

        performAndAssert(requestDto, "channel", ValidationErrorMessages.CHANNEL_NULL_VALUE);

    }

    @DisplayName("Fail with incorrect channel value")
    @Test
    void shouldFailWithIncorrectChannel() throws Exception {
        var requestDto = new LaunchRequestDto("providerGameId", "sessionKey", "incorrect", "ua",
                "https://lobby.com/path?param=1");

        performAndAssert(requestDto, "channel", ValidationErrorMessages.CHANNEL_INCORRECT_VALUE);
    }

    @DisplayName("Fail with empty/null lang value")
    @ParameterizedTest
    @NullAndEmptySource
    void shouldFailWithBlankOrNullLang(String lang) throws Exception {
        var requestDto = new LaunchRequestDto("providerGameId", "sessionKey", "mobile", lang,
                "https://lobby.com/path?param=1");

        performAndAssert(requestDto, "lang", ValidationErrorMessages.LANG_EMPTY_OR_NULL_VALUE);
    }

    @DisplayName("Fail with empty/null lobbyUrl value")
    @ParameterizedTest
    @NullAndEmptySource
    void shouldFailWithBlankOrNullLobbyUrl(String lobbyUrl) throws Exception {
        var requestDto = new LaunchRequestDto("providerGameId", "sessionKey", "mobile", "ua", lobbyUrl);

        performAndAssert(requestDto, "lobbyUrl", ValidationErrorMessages.LOBBY_URL_EMPTY_OR_NULL_VALUE);
    }

    @DisplayName("Fail with incorrect lobbyUrl value")
    @ParameterizedTest
    @ValueSource(strings = { "incorrect" })
    void shouldFailWithIncorrectLobbyUrl(String lobbyUrl) throws Exception {
        var requestDto = new LaunchRequestDto("providerGameId", "sessionKey", "mobile", "ua", lobbyUrl);

        performAndAssert(requestDto, "lobbyUrl", ValidationErrorMessages.LOBBY_URL_INCORRECT_VALUE);
    }

    private void performAndAssert(LaunchRequestDto requestDto, String providerGameId2,
            String providerGameIdEmptyOrNullValue) throws Exception {
        mockMvc.perform(post("/launch/real").content(new ObjectMapper().writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.validationErrors[0].field").value(providerGameId2))
                .andExpect(jsonPath("$.validationErrors[0].message").value(providerGameIdEmptyOrNullValue));
    }
}