package io.pmlab.hab88launcher.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.pmlab.hab88launcher.model.LaunchRequestDto;

class LaunchServiceTest {

    private LaunchService service;

    @BeforeEach
    void init() {
        service = new LaunchService("https://haba88.com/", "c07552ae-a65c-4d7d-93dd-10add80817be");
    }

    @Test
    public void shouldReturnRealModeUrl() {
        var requestDto = new LaunchRequestDto("gameId", "casino-session-token", "mobile", "ua",
                "https://lobby.com/path?param=1");
        var launchResponseDto = service.provideUrl(requestDto);
        assertEquals(launchResponseDto.getUrl(),
                "https://haba88.com/?brandid=gameId&brandgameid=c07552ae-a65c-4d7d-93dd-10add80817be"
                        + "&token=casino-session-token&mode=real&locale=ua&lobbyurl=https%3A%2F%2Flobby.com%2Fpath%3Fparam%3D1");
    }

    @Test
    public void shouldReturnFunModeUrl() {
        var requestDto = new LaunchRequestDto("gameId", null, "mobile", "ua", "https://lobby.com/path?param=1");
        var launchResponseDto = service.provideUrl(requestDto);
        assertEquals(launchResponseDto.getUrl(),
                "https://haba88.com/?brandid=gameId&brandgameid=c07552ae-a65c-4d7d-93dd-10add80817be"
                        + "&mode=fun&locale=ua&lobbyurl=https%3A%2F%2Flobby.com%2Fpath%3Fparam%3D1");
    }
}